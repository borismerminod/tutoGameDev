/*
Boris Merminod 25/09/2017 

Introduction à la gestion de plusieurs écran. Si l'utilisateur du programme utilise plusieurs écran, la SDL peut gérer ces écrans de manière à ce que les fenêtre ne soit pas perturbé par le passage d'un écran à l'autre

On défini ainsi deux variable globales l'une permettant de gérer le nombre d'écran et l'autre les limite de chaque écran via une structure SDL_Rect *.


La classe LWindow va se voir ajouter un attribut mWindowDisplayID pour gérer l'ID de l'écran qui contient la fenêtre
La méthode handleEvent contient un flag switchDisplay qui permet lorsque l'on presse une touche du clavier de switcher la position de la fenêtre d'un écran à l'autre

La fonction init va utiliser la fonction SDL_GetNumVideoDiplays pour connaître le nombre d'écran présent. On va ensuite allouer une table de SDL_Rect à partir de gDisplayBounds contenir les dimensions de chaque écran. Ensuite la fonction SDl_GetBounds permet de récupérer ces dimensions

Code source tiré de : http://lazyfoo.net/tutorials/SDL/37_multiple_displays/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
//#include <string.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

class LWindow
{
	public :
		//Initialisation de l'objet
		LWindow();
		
		//Création de fenêtres
		bool init();
		
		//Gestion des window events
		void handleEvent(SDL_Event & e);
		
		//Rendu du contenu des fenêtres
		void render();
		
		//Focus sur la fenêtre
		void focus();
		
		//Libération de la mémoire allouée
		void free();
		
		//Accesseur des dimensions de la fenêtre
		int getWidth();
		int getHeight();
		
		//Window focii
		bool hasMouseFocus();
		bool hasKeyboardFocus();
		bool isMinimized();
		bool isShown();
		
	private :
	
		//Structure de données de type fenêtre
		SDL_Window* mWindow;
		SDL_Renderer * mRenderer;
		int mWindowID;
		int mWindowDisplayID;
		
		//Dimensions de la fenêtre
		int mWidth;
		int mHeight;
		
		//Window focus
		bool mMouseFocus;
		bool mKeyboardFocus;
		bool mFullScreen;
		bool mMinimized;
		bool mShown;
};


// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
LWindow gWindow;

//Display data
int gTotalDisplays = 0;
SDL_Rect* gDisplayBounds = NULL;


// ~~~~~ Méthodes de la classe LWindow ~~~~~

//Initialisation de l'objet LWindow
LWindow::LWindow()
{
	mWindow = NULL;
	mRenderer = NULL;
	
	mMouseFocus = false;
	mKeyboardFocus = false;
	mFullScreen = false;
	mMinimized = false;
	mShown = false;
	
	mWindowID = -1;
	mWindowDisplayID = -1;
	mWidth = 0;
	mHeight = 0;
}

//Création d'une fenêtre et de son renderer
bool LWindow::init()
{
	mWindow = SDL_CreateWindow("Tutoriel SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(mWindow != NULL)
	{
		mMouseFocus = true;
		mKeyboardFocus = true;
		mWidth = SCREEN_WIDTH;
		mHeight = SCREEN_HEIGHT;
		
		//Création du renderer
		mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if(mRenderer == NULL)
		{
			fprintf(stderr, "init : Echec de création du renderer ! SDL Error : %s\n", SDL_GetError());
			SDL_DestroyWindow(mWindow);
			mWindow = NULL;
		}
		else
		{
			SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
			//Grab window identifier
			mWindowID = SDL_GetWindowID(mWindow);
			mWindowDisplayID = SDL_GetWindowDisplayIndex(mWindow);
			
			//Flag d'ouverture de la fenêtre
			mShown = true;
		}
	}
	else
	{
		fprintf(stderr, "init : Impossible de créer la fenêtre ! SDL Error : %s\n", SDL_GetError());
	}
	
	return mWindow != NULL && mRenderer != NULL;	
}

//Gestion des window events

void LWindow::handleEvent(SDL_Event & e)
{
	bool updateCaption = false;
	//Un window event se produit 
	if(e.type == SDL_WINDOWEVENT && e.window.windowID == mWindowID)
	{
		switch(e.window.event)
		{
			//La fenêtre est bougée
			case SDL_WINDOWEVENT_MOVED :
				mWindowDisplayID = SDL_GetWindowDisplayIndex(mWindow);
				updateCaption = true;
				break;
			//La fenêtre apparaît
			case SDL_WINDOWEVENT_SHOWN :
				mShown = true;
				break;
				
			//La fenêtre disparaît
			case SDL_WINDOWEVENT_HIDDEN :
				mShown = false;
				break;
				
			//Changement de dimension d'une fenêtre
			case SDL_WINDOWEVENT_SIZE_CHANGED :
				mWidth = e.window.data1;
				mHeight = e.window.data2;
				SDL_RenderPresent(mRenderer);
				break;
			
			//Repaint on exposure
			case SDL_WINDOWEVENT_EXPOSED :
				SDL_RenderPresent(mRenderer);
				break;
			
			// La souris entre dans la fenêtre
			case SDL_WINDOWEVENT_ENTER :
				mMouseFocus = true;
				updateCaption = true;
				break;
			
			//La souris sort de la fenêtre
			case SDL_WINDOWEVENT_LEAVE :
				mMouseFocus = false;
				updateCaption = true;
			
			//La fenêtre possède le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_GAINED :
				mKeyboardFocus = true;
				updateCaption = true;
				break;
			
			//La fenêtre perd le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_LOST :
				mKeyboardFocus = false;
				updateCaption = true;
				break;
			
			//La fenêtre est réduite
			case SDL_WINDOWEVENT_MINIMIZED :
				mMinimized = true;
				break;
			
			//La fenêtre est agrandie 
			case SDL_WINDOWEVENT_MAXIMIZED : 
				mMinimized = false;
				break;	
			
			//La fenêtre est restaurée
			case SDL_WINDOWEVENT_RESTORED :
				mMinimized = false;
				break;
			
			//Cache lors d'une fermeture
			case SDL_WINDOWEVENT_CLOSE :
				SDL_HideWindow(mWindow);
				break;
		}
	}
	else if(e.type == SDL_KEYDOWN)
	{
		//flag du changement d'affichage
		bool switchDisplay = false;
		
		//Cycle through displays on up/down
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				++mWindowDisplayID;
				switchDisplay = true;
				break;
			case SDLK_DOWN :
				--mWindowDisplayID;
				switchDisplay = true;
				break;
		}
			
		//Si une mise à jour de l'affichage est nécessaire
		if(switchDisplay)
		{
			//Bound display index
			if(mWindowDisplayID < 0)
				mWindowDisplayID = gTotalDisplays - 1;
			else if (mWindowDisplayID >= gTotalDisplays)
				mWindowDisplayID = 0;
			
			//Move window to center of next display
			SDL_SetWindowPosition(mWindow, gDisplayBounds[mWindowDisplayID].x + (gDisplayBounds[mWindowDisplayID].w-mWidth)/2, gDisplayBounds[mWindowDisplayID].y + (gDisplayBounds[mWindowDisplayID].h - mHeight) /2);
			updateCaption = true;
		}
	}
		
		
	//Mise à jour du window caption avec de nouvelle données
	if(updateCaption)
	{
		std::stringstream caption;
		caption << "SDL Tutorial - MouseFocus" << ((mMouseFocus)?"On":"Off") << " KeyboardFocus:"<<((mKeyboardFocus)?"On":"Off");
		SDL_SetWindowTitle(mWindow, caption.str().c_str());
	}
	
}

//Focus sur une fenêtre
void LWindow::focus()
{
	//Restaure la fenêtre si nécessaire
	if(!mShown)
		SDL_ShowWindow(mWindow);
	//Place la fenêtre devant
	SDL_RaiseWindow(mWindow);
}

void LWindow::render()
{
	if(mMinimized == true)
		return;
	//Clear the screen
	SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(mRenderer);
	
	//Update screen
	SDL_RenderPresent(mRenderer);
}

//Libérer la mémoire allouée pour créer la fenêtre
void LWindow::free()
{
	if(mWindow != NULL)
		SDL_DestroyWindow(mWindow);
	
	mMouseFocus = false;
	mKeyboardFocus = false;
	mWidth = 0;
	mHeight = 0;
}

//Accesseur de la longueur de la fenêtre
int LWindow::getWidth()
{
	return mWidth;
}

//Accesseur de la hauteur de la fenêtre
int LWindow::getHeight()
{
	return mHeight;
}

//Accesseur la fenêtre possède-t'elle le focus de la souris ?
bool LWindow::hasMouseFocus()
{
	return mMouseFocus;
}

//Accesseur la fenêtre possède-t'elle le focus du clavier
bool LWindow::hasKeyboardFocus()
{
	return mKeyboardFocus;
}

//Accesseur la fenêtre est-elle réduite ?
bool LWindow::isMinimized()
{
	return mMinimized;
}

//Accesseur la fenêtre est-elle visible
bool LWindow::isShown()
{
	return mShown;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Obtenir le nombre de display
	gTotalDisplays = SDL_GetNumVideoDisplays();
	if(gTotalDisplays < 2)
		fprintf(stderr, "init : Attention seulement un display connecté");
	
	//Get bounds of each display
	gDisplayBounds = new SDL_Rect[gTotalDisplays];
	for(int i = 0; i<gTotalDisplays; i++)
		SDL_GetDisplayBounds(i, &gDisplayBounds[i]);
	
	//Création d'une fenêtre	
	if(gWindow.init() == false)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Détruire la fenêtre
	gWindow.free();
	
	//Désallouer les bounds
	delete [] gDisplayBounds;
	gDisplayBounds = NULL;
	
	//Quitter la SDL
	SDL_Quit();
}


// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;

	while(!quit)
	{
		
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			//Gestion des événements de la fenêtre
			gWindow.handleEvent(e);
			
		}
		
		//Mise à jour de la fenêtre
		gWindow.render();
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

