/*
Boris Merminod le 15/01/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/17_vertex_buffer_objects/index.php
*/

#ifndef LVERTEX_POS_2D_H
#define LVERTEX_POS_2D_H

#include "LOpenGL.h"

struct LVertexPos2D
{
	GLfloat x;
	GLfloat y;
};

#endif
