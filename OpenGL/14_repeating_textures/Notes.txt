Boris Merminod le 10/01/2019

	Introduction à la répetition des textures. Nous allons ici créer un scrolling tilling effect en utilisant une méthode de répétition d'une texture par manipulation de ces coordonnées
	
LTexture.cpp :

		On commence par un paramètre global
	
		GLenum DEFAULT_TEXTURE_WRAP = GL_REPEAT;
	
	En plus du filtre de texture, on peut indiquer un autre paramètre indiquant comment la textures "wraps". En principe dans les cas classique les coordonnées de textures sont représenté entre 0.0 et 1.0. Celà dit on peut augmenter cette coordination jusqu'à 2.0 ce qui signifie que 200% de la texture va être cartographié. cela signifie que la texture va être représenté deux fois le long de l'axe ou l'on a indiqué la coordonnée 2.0. Pour cela on doit indiquer  un Default Texture Wrap (si j'ai bien compris c'est déjà implicitement réglé sur GL_REPEAT, ici on ce contente de le rendre explicite).
	
	Dans notre fonction loadTextureFromPixel32, on va indiquer l'utilisation du Texture Wrap dans l'axe S et dans l'axe T via l'utilisation de glTexParameteri : 
	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);	
		
LUtil.cpp :

	On commence ici par déclarer quelques variables globales. En premier la texture qui va nous servir de tuile (on dit en anglais to tile a texture). Ensuite il y a des offsets qui vont nous aider à dérouler la texture. Enfin on a une variable indiquant comment nous allons wrap notre texture
	
	//L'image qui va être répetée
	LTexture gRepeatingTexture;

	//Texture offset
	GLfloat gTexX = 0.f, gTexY = 0.f;

	//Texture wrap type
	int gTextureWrapType = 0;

	Notre fontion loadMedia() va charger notre texture et... c'est tout
	
	La fonction update() va mettre à jour les valeurs de nos variable d'offset pour dérouler notre texture. Le tout est limité par un seuil qui une fois dépasser va réinitialiser les coordonnées de notre texture ce qui nous donne plus clairement un scrolling en diagonale en boucle infinie
	
	La fonction render va faire le calcul suivant : 
	
	GLfloat textureRight = (GLfloat)SCREEN_WIDTH / (GLfloat) gRepeatingTexture.textureWidth();
	GLfloat textureBottom = (GLfloat)SCREEN_HEIGHT / (GLfloat)gRepeatingTexture.textureHeight();
	
Si on se dit que ma texture fait 128 pixels et que mon écran fasse 256 pixels de large, en divisant 256/128 j'obtiens 2.0, grâce à se résultat, on va mapper deux fois la texture sur l'axe x/s. Se calcul va aussi être  réalisé pour l'axe y/t.
	Après ce calcul, on va attacher notre texture via glBindingTexture, puis on va switcher vers la texture matrix via glMatrixMode(). La texture matrix peut être utilisée pour effectuer des transformations sue les coordonnées des textures, un peu comme quand on utilise la matrice modelview pour transformer des coordonnées de sommets. Cette matrice va être utilisée comme matrice d'identité via glLoadIdentity()
	Après avoir initialisée la matrice de texture, on va utiliser glTranslatef pour se positionner par rapport à la mise à jour de la valeur de l'offset. ici la valeur 1.0 d'un paramètre ne signifie pas un pixel, mais 100% de la texture, ce calcul est obtenu en divisant l'offset par la largeur en x, puis la hauteur en y. Enfin on va effectuer le rendu du quad avec les coordonnées transformée des textures :
	
	glTranslatef(gTexX / gRepeatingTexture.textureWidth(), gTexY / gRepeatingTexture.textureHeight(), 0.f);
	
	//Rendu
	glBegin(GL_QUADS);
		glTexCoord2f(0.f, 0.f); glVertex2f(0.f, 0.f);
		glTexCoord2f(textureRight, 0.f); glVertex2f(SCREEN_WIDTH, 0.f);
		glTexCoord2f(textureRight, textureBottom); glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);
		glTexCoord2f(0.f, textureBottom); glVertex2f(0.f, SCREEN_HEIGHT);
	glEnd();

	Il nous reste la fonction handleKeys(). en pressant sur 'q' on va changer via glTexParameteri le type de texture wrapping de GL_REPEAT à GL_CLAMP. Dans le cas de GL_REPEAT, en rencontrant une coordonnée de texture entre 0 et 1 le système va continuer son mapping de texture et le répéter. Dans le cas de GL_CLAMP, le mapping de texture va s'arrêter au-delà de 0 et 1 
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/14_repeating_textures/index.php
