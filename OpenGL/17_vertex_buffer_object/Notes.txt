Boris Merminod le 15/01/2019


	Introduction au Vertex Buffer Object. Il est plus efficace de traiter les sommets dans une table de sommets. Pour rendre le tout encore plus efficace, on peut stocker notre table dans un Buffer nommé le Vertex Buffer Object, permettant de stocker nos sommet directement dans le GPU une bonne fois pour toutes. Cela permet au GPU d'avoir ces données à portée et de l'utilisée dès qu'il en a besoin.

LUtil.cpp :

	Au sommet de notre LUtil.cpp, on va commencer par définir quelques variables globales :
	
	//Les sommets d'un Quad
	LVertexPos2D gQuadVertices[4];

	//Indices des sommets
	GLuint gIndices[4];

	//Buffer des sommets
	GLuint gVertexBuffer = 0;

	//Index buffer
	GLuint gIndexBuffer = 0;

grosso modo c'est comme au chapitre précédent sauf qu'on ajoute cette fois une variable pour gérer des indices
	Dans le chapitre précédent, on utilise la fonction glDrawArrays(), pour dessiner notre forme géométrique à partir des sommets mémorisés. La particularité de cette fonction est qu'elle va dessiner dans l'ordre des indices de la table. Dans ce code on va utiliser une fonction qui peut tracer les sommets dans n'importe quel ordre. Avec cette fonction on peut même effectuer deux fois le rendu d'un même sommet. Ainsi dans cette exemple on a besoin d'une table d'indice pour savoir dans quel ordre on va tracer les sommets (C'est la raison d'être de gIndices).
	On dispose donc d'une table de sommets et d'une table d'indices. L'idée derrière ce chapitre est de les stocker dans le GPU à l'aide d'un buffer. Pour cela on va créer un Buffer Object qui sera lié à nos table via un ID. gVertexBuffer est donc l'ID pour notre table de sommets et gIndexBuffer est l'ID pour notre table des indices.
	
	Dans notre fonction loadMedia(), on va définir nos coordonnées de sommets comme au chapitre précédent, puis on va assigner des valeurs à notre table d'indices. Cette table sera lue dans l'ordre, chacune des valeur représentant un indice, il suffit de changer la valeur associée à ces indices pour tracer notre figure dans un ordre différent.
	Il y a un intérêt à faire ça, en 3D si je veux dessiner un cube, possédant 6 faces multiplié par 4 sommets, j'obtiens 24 sommets pour tracer mon cubes. Sauf que en réalité un cube ne possède que 8 coins différents. Si je veux optimiser mon tracé il me suffit d'indiquer les coordonnées des 8 coins de mon cube pour réussir à le tracer.
	Viens ensuite l'étape d'associer nos tables à un buffer. On va utiliser pour ça trois fonctions en commençant par notre table des sommets:
	- glGenBuffers() va créer le Buffer (on lui passe l'adresse de la variable qui contiendra l'ID en paramètre)
	- Ensuite on va attacher le Buffer via glBindBuffer() en lui indiquant en paramètre que je veux lui passer une table (GL_ARRAY_BUFFER) ainsi que l'ID du Buffer en second paramètre.
	- Enfin on va associer notre table à notre Buffer via glBufferData() en lui passant en paramètre, le fait qu'il s'agisse d'une table (GL_ARRAY_BUFFER), la taille en mémoire de cette table (taille * sizeof(type)), la table en question (son adresse), et un flag qui indique que les données seront envoyée au Buffer une fois mais dessinée plusieurs fois (GL_STATIC_DRAW).
	Pour faire la même chose avec notre table d'index, on va procédé de la même façon à l'aide des fonctions précédentes. On indiquera juste au système qu'il s'agit d'une table d'index (GL_ELEMENT_ARRAY_BUFFER).
	
	Dans la fonction render(), on va faire un clear de l'écran, puis on va activé le rendu du vertex array comme dans le chapitre précédent. Ensuite le rendu va être effectué via notre VBO (Vertex Buffer Object). Pour ça on va appeler glBindBuffer pour attacher notre Buffer, puis on va set nos vertex datas via glVertexPointer (Il va les récupérer directement du Buffer du coup). On souhaite que le rendu commence en prenant les données des sommets depuis le début du VBO, du coup on lui passe l'adresse à NULL dans ce but.
	Ensuite on va vouloir connaître les indices pour savoir dans quel ordre on effectue le rendu de notre figure. On va donc récupérer le buffer de notre IBO via glBindBuffer. Il ne nous reste plus qu'à dessiner le tout.
	Le dessin est effectué par glDrawElements(). La fonction prends en argument : le type de géométrie à tracer, le nombre d'éléments à tracé (en l'occurrence 4 pour un carré), en troisième argument on a le type de données pour nos indices d'IBO (GL_UNSIGNED_INT), le quatrième élément est l'adresse de notre table d'indices. Cette adresse est utilisée via notre IBO qui est actuellement Bind, on souhaite également lire cet IBO depuis le début, c'est pourquoi on utilise comme pour le VBO le NULL indiquant qu'on prend cette table depuis le début.
	De ce que je comprend, on pourrait ne pas bind notre IBO et passer à notre glDrawElements l'adresse de notre table d'index (du coup c'est comme si on travaillait sans Buffer ce qui signifie que les données de sommet sont envoyé au GPU à chaque appel de la fonction render). Il faut savoir que cette méthode de Vertex Array est obsolète dans les version de OpenGL au-dessus de 3.0+.
	Quand le dessin est fait on désactive notre vertex array et on met à jour l'écran.
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/17_vertex_buffer_objects/index.php	
