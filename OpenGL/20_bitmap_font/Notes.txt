Boris Merminod le 23/04/2019

Introduction au Bitmap Font

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php

Introduction

	Introduction à la gestion d'un bitmap font à l'aide d'OpenGL. OpenGL ne contient aucune fonction pour gérer le rendu textuel. Cependant on peut utiliser les sprite sheet pour effectuer le rendu textuel en image.
	Un bitmap font est un type spécial de sprite sheet. L'idée va donc d'utiliser le clipping pour effectuer le rendu des lettres qui nous intéresse dans l'ordre qui nous intéresse
	
LSpriteSheet.h :

	On va ajouter à la classe LSpriteSheet la possibilité de changer l'origine de rendu des sprites. Dans le chapitre précédent, ce rendu était effectué depuis le centre du sprite. Ici le rendu sera géré depuis le coin supérieur gauche des sprites
	
	On va ensuite changer le prototype de la fonction generateBuffer :
	
bool generateDataBuffer(LSpriteOrigin origin = LSPRITE_ORIGIN_CENTER);

On indique comme ça que l'origine de rendu par défaut est le centre. Cela va affecter la façon des les vertex datas seront générés

LSpriteSheet.cpp :

	Dans la fonction generateDataBuffer(), on va générer notre VBO et notre IBO comme avant en rajoutant la capacité de réaliser le parsing des rectangles de clipping. On ajoute cette fois des variables additionnelles pour set les sommets supérieure, inférieurs, gauche et droits.
	Pour chaque sprite on va setter les indices puis on va calculer le décalage en y du haut vers le bas, et le décalage en x de la gauche vers la droite pour notre vertex data. Cela va dépendre de l'origine de départ choisie.
	Ensuite on va setter les coordonnées de nos sommets et des textures ainsi que les indices par sprites comme celà a été réalisé dans le chapitre précédent. Ce qui change c'est comment sont placés les positions des sommets dont nous allons effectuer le rendu.
	Pour finir on va générer notre VBO et reporter les erreurs si elles doivent survenir
	
LFont.h :

	On ajoute ici une classe LFont qui va héritée de LSpriteSheet. 
	La classe se compose d'un constructeur et destructeur normal, ensuite vient la fonction loadBitmap() qui va charger une image puis en effectuer un parsing en plusieurs sprites pour chaque lettre du font qui seront convertie en textures.
	La fonction freeFont() va libérer la mémoire allouée pour gérer le bitmap font.
	La fonction renderText() va effectuer le rendu d'un texte à partir du bitmap font.
	La classe possèdent 3 attributs. mSpace indique de combien de pixel en x il faut bouger quand un ' ' est rencontré. mLineHeight représente la distance entre la lettre la plus haute en pixel et la lettre la plus basse en pixels. mNewLine représente de combien de pixel on va se déplacer en y quand on rencontre un '\n'
	
LFont.cpp :

	Je passe un peu sur le constructeur et le destructeur qui ne font rien de fou.
	
	La fonction loadBitmap() a elle un grand intérêt. La fonction a besoin d'un path vers un fichier .bmp contenant du noir, du blanc et des nuances de gris. Le noir est ici la couleur de fond du .bmp si bien qu'un autre type de font ne fonctionnerait pas avec notre fonction. On commence donc par faire un freeFont() pour s'assurer qu'il n'y ait pas de font déjà chargé. Ensuite on va charger via loadPixelsFromFile notre font dans des pixels dont nous allons effectuer le parsing.
	Notre parser fonctionne selon l'idée que nos caractères sont séparés selon une grille de 16x16. Notre font nouvellement chargé va donc être divisé en width et en height par 16 ce qui nous donnera les dimensions de chaque case en pixels. Notre fonction utilise plusieurs variables :
- top, bottom et aBottom sont utilisés pour gérer l'espacement entre les caractères
- Les coordonnées en pixels sont représentées par pX et pY 
- Les coordonnées en pixels relative à chaque case de la grille sont représentés par bX et bY
- currentChar : garde une trace du code ASCII utilisé pour la case courante de la grille qui se prend le parsing
- nextClip : est le rectangle de clipping pour le sprite de notre caractère courant qui se prend le parsing
	Notre font va ensuite être parcouru ligne par ligne et pour chaque ligne, colonne par colonne. le parcour commence par le calcul du décalage de base pour chaque cellule ce qui va représenter les variable bX et bY initialisé à leur valeur "zéro". Le rectangle de clip du prochain clip est lui initialisé comme faisant la taille entière d'une cellule.
	Une cellule est parcouru ligne par ligne et pour chaque ligne on parcour les pixels colonne par colonne. Chaque pixel est alors analysé jusqu'à ce que l'on tombe sur un pixel dont la valeur RGBA ne correspond pas à ce qui a été défini en couleur de fond. Lors de cette détéction on choppe ainsi l'offset en x de notre caractère représentant son côté gauche.
	En parcourant notre cellule ligne par ligne et colonne par colonne mais en partant par la droite, selon la même méthode, on fini par trouver la largeur de notre caractère.
	Au sujet de la hauteur des caractères, on fonctionne un peu différement. Dans chacune de nos case, il y a un gap entre le sommet de la case et le haut du caractère. Pour que notre rectangle de clip soit le mieux adapté pour le plus de caractère possible, on va rechercher le caractère qui à la hauteur la plus grande et s'en servir comme hauteur par défaut pour l'ensemble des caractères de notre font. Cette hauteur doit être mesurée des deux côté, ainsi on va rechercher le point le plus haut pour le caractère ayant la hauteur la plus élevée, mais aussi le point le plus bas pour le caractère ayant la base la plus basse.
	La base du A est prise à part pour une raison que je n'ai pas bien comprise (je crois que c'est l'habitude du programmeur que je suis en train d'étudier).
	Le parsing d'une cellule va se terminer par l'ajout du sprite du caractère dans notre clip sprites. Ensuite on va incrémenter la valeur de currentChar qui contient le code ASCII du prochain caractère que l'on va parser. Et on v poursuivre ceci pour les 256 caractères de notre font.
	Lorsque l'opération de parsing est totalement terminée, on va passer en revue les rectangle de clip pour attribuer le point le plus haut et la taille en y du plus grand caractère du font sur les 256 caractères représentés.
	L'utilisation d'OpenGL nous permet de travailler sur les caractères de notre font. On pourrait par exemple lisser chaque sprite pour donner un effet moins plat à notre police d'écriture. Ce que l'on peut ainsi faire c'est utiliser la valeur brightness de chaque pixel comme égale à sa valeur alpha. Ainsi plus le pixel est sombre plus il ressortira fortement par rapport au fond. Comme notre bitmap font est une échelle de gris, il nous suffit juste de prendre la valeur du component Red comme  réprensatatif du brightness. Ensuite pour chaque pixel on va setter la valeur RGB vers le blanc et la seul chose qui shading sera sa transparence.
	Une fois le parsing terminé ainsi que le calcul des rectangle de clip, on va générer la texture ainsi que le VBO data.
	La texture wrap de notre texture va être paramétré sur GL_CLAMP_TO_BORDER, pour éviter que les bord des sprites ne soit découper d'une mauvaise façon.
	Ensuite on va calculer la valeur de l'espace. De base la valeur est fixée sur la moitié de la largeur d'une cellule. La valeur du retour charriot est aussi calculé par la distance entre le point de pixel du caractère le plus haut et la ligne de base de la cellule (je crois). Enfin la hauteur de ligne est calculée comme étant l'espace entre le pixel le plus haut du caractère le plus haut et le pixel le plus bas du caractère le plus bas.
	
	La fonction freeFont() va appeler la méthode freeTexture définie dans la classe mère LSpriteSheet
	
	Vient ensuite la fonction renderText(), qui ressemble à une version un peu spéciale de la fonction renderSprite de la classe LSpriteSheet. La fonction va initialiser les position à dessiner, effectuer un translate vers ces position pour effectuer le rendu des différent point, attacher ensuite la texture puis set les VBO data.
	L'avantage d'utiliser LFont est que l'on va attacher la texture une fois, puis effectuer le rendu plusieurs fois à partir de cette attache. Le Binding de texture dans OpenGL est une opération très coûteuse, il est donc important de bien tirer parti d'une texture attachée pour éviter des échanges permanent avec la VRAM.
	La fonction va parcourir le string qui lui est passé en paramètre pour effectuer le rendu du caractère adéquat correspondant à chaque caractère. Si un espace est trouvé, on va faire une translation pour montrer l'espace et décaler le prochain point dont il faudra effectuer le rendu. Le retour charriot réinitialise la position de rendu au début d'une ligne et effectuer un espace vers le bas en y. Pareil le prochain point de rendu est décalé.
	Quand on tombe sur un caractère, on va récupérer son code ASCII puis attacher le sprite du caractère en question via l'IBO ce qui nous permettra d'en effectuer le rendu. Puis on va faire une translation vers le prochain point de rendu. Le tout ce répète jusqu'à ce qu'il n'y ait plus rien à afficher
	
LUtil.cpp :

	La fonction loadMedia() va simplement servir à charger notre bitmap font.
	
	La fonction render() va effectuer le rendu de notre texte en appelant la méthode adéquat de la classe LFont


	
		
