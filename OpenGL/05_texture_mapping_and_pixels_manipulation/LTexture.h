/*
	Boris Merminod le 14/05/2018
	
	Définition d'une classe de gestion des textures : LTexture.
	
		La classe LTexture se compose : d'un constructeur et d'un destructeur, d'une méthode bool loadTextureFromPixels32() qui va prendre les données de pixels afin de les convertir en texture, d'une méthode freeTexture() qui va libérer la mémoire allouée pour gérer un objet LTexture, d'une méthode render() qui va récupérer notre cartographie de texture afin d'en effectuer le rendu sous la forme d'un carré, et de trois accesseur getTextureID(), textureWidth() et textureHeight() permettant de récupérer des informations sur notre texture
		
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/05_texture_mapping_and_pixel_manipulation/index.php
*/

#ifndef LTEXTURE_H
#define LTEXTURE_H

#include "LOpenGL.h"
#include <stdio.h>

class LTexture
{
	public :
		LTexture();
		/*
		Pré-conditions :
		-None
		Post-conditions :
		- Initialise les attributs membres
		Side effects :
		- None
		*/
		
		~LTexture();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Libère la mémoire allouée pour gérer un objet de texture
		Side effects:
		- None
		*/
		
		bool loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Création d'une texture à partir de pixels donnés
		- Reporte les erreurs à la console si la texture n'a pas pu être créée
		Side Effects: 
		- Attache une texture NULL
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Suppression des texutres si elles existes
		- Mise en place d'un texture ID à 0
		Side Effects :
		- None
		*/
		
		void render(GLfloat x, GLfloat y);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Une matrice modelview active
		Post-conditions :
		- opération de translation à une position données puis rendu de carré texturés
		Side  effects :
		- Associe à la texture membre une ID
		*/
		
		GLuint getTextureID();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne le nom et l'ID de la texture
		Side effects :
		- None
		*/
		
		GLuint textureWidth();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- retourne la largeur (width) de la texture
		Side effects :
		- None
		*/
		
		GLuint textureHeight();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne la hauteur de la texture (height)
		Side effects :
		- None
		*/
		
	private :
		//Nom de la texture
		GLuint mTextureID;
		
		//Dimensions de la texture
		GLuint mTextureWidth;
		GLuint mTextureHeight;	
};

#endif
