/*
Boris merminod le 17/05/2018

	On commence par inclure les bibliothèque de DevIL : IL/il.h et IL/ilu.h. 
	
	Dans la fonction initGL, on va initialiser OpenGL normalement puis la bibliothèque DevIL via ilInit(). La fonction ilClearColor() va initialiser une clear color à rendre en transparent ici le blanc. Après l'initialisation de DevIL on check les erreurs. 

	La fonction loadMedia() va être chargée d'appeler la méthode loadTextureFromFile via un objet gloabal LTexture gLoadedTexture.
	
	La fonction render va ensuite effectuer le rendu de la texture via la méthode render de l'objet gLoadedTexture.

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/06_loading_a_texture/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Checkerboard texture
LTexture gLoadedTexture;


bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gLoadedTexture.loadTextureFromFile("sprite/texture.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Calcul du décalage centré
	GLfloat x = (SCREEN_WIDTH - gLoadedTexture.textureWidth()) /
	2.f;
	GLfloat y = (SCREEN_HEIGHT - gLoadedTexture.textureHeight()) /
	2.f;
	
	//Rendu de la texture checkerboard
	gLoadedTexture.render(x, y);
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
