/*
	Boris Merminod le 17/05/2018
	
	Ajout à la class LTexture de la méthode loadTextureFromFile(). OpenGL ne dispose pas de fonction pour manipuler des images, pour se faire on utilise une bibliothèque annexe DevIL (voir le détail d'installation dans les notes)
		
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/06_loading_a_texture/index.php
*/

#ifndef LTEXTURE_H
#define LTEXTURE_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>
#include <cstdlib>

class LTexture
{
	public :
		LTexture();
		/*
		Pré-conditions :
		-None
		Post-conditions :
		- Initialise les attributs membres
		Side effects :
		- None
		*/
		
		~LTexture();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Libère la mémoire allouée pour gérer un objet de texture
		Side effects:
		- None
		*/
		
		bool loadTextureFromFile(std::string path);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Librairie DevIL initialisée
		Post-conditions :
		- Création d'une texture à partir d'un fichier
		- Report des erreur à la sortie stderr
		Side Effects :
		- Association d'une texture NULL 
		*/
		
		bool loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Création d'une texture à partir de pixels donnés
		- Reporte les erreurs à la console si la texture n'a pas pu être créée
		Side Effects: 
		- Attache une texture NULL
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Suppression des texutres si elles existes
		- Mise en place d'un texture ID à 0
		Side Effects :
		- None
		*/
		
		void render(GLfloat x, GLfloat y);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Une matrice modelview active
		Post-conditions :
		- opération de translation à une position données puis rendu de carré texturés
		Side  effects :
		- Associe à la texture membre une ID
		*/
		
		GLuint getTextureID();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne le nom et l'ID de la texture
		Side effects :
		- None
		*/
		
		GLuint textureWidth();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- retourne la largeur (width) de la texture
		Side effects :
		- None
		*/
		
		GLuint textureHeight();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne la hauteur de la texture (height)
		Side effects :
		- None
		*/
		
	private :
		//Nom de la texture
		GLuint mTextureID;
		
		//Dimensions de la texture
		GLuint mTextureWidth;
		GLuint mTextureHeight;	
};

#endif
