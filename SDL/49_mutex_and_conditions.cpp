/*
Boris Merminod le 04/03/2018

Introduction au Mutex et Conditions. Les Mutex (Mutually exclusive) peuvent vérrouiller un buffer à la manière d'un sémaphore ou d'une atomic operations. Le plus est qu'ils permettent via des Conditions aux threads de communiquer entre eux et de leur dire quand le buffer est de nouveau disponible.

Voici les 4 fonctions que nous allons utiliser :
	//Our worker functions 
	int producer( void* data ); 
	int consumer( void* data ); 
	void produce(); 
	void consume();
Nous aurons donc un thread pour remplir le buffer et un thread pour le vider. Les Mutex vont permettre d'éviter que les deux threads ne tente d'intéragir avec le buffer en même temps

Voici les structures de données utilisées :
	//The protective mutex 
	SDL_mutex* gBufferLock = NULL; 
	
	//The conditions 
	SDL_cond* gCanProduce = NULL; 
	SDL_cond* gCanConsume = NULL; 
	
	//The "data buffer" 
	int gData = -1; 

L'utilisation des mutex nécessite une allocation de mémoire : SDL_CreateMutex permet d'allouer et SDL_DestroyMutex de libérer. De même pour les conditions SDL_CreateCond() pour en créer et SDL_DestroyCond pour les libérer.

La fonction thread producer va modifier à l'aide de la fonction produce le contenu du buffer pour le remplir. La fonction thread consumer va à l'aide de la fonction consume vider le buffer.
Chaque thread contient une partie SDL_LockMutex et SDL_UnlockMutex ou le thread va tenter d'intéragir avec le buffer, mais pour cela il va controler via la fonction SDL_CondWait si le buffer est disponible en vérifiant la condition associée. Tant que ce n'est pas le cas le thread reste en attente. Dès qu'il lui est possible de travailler il effectue son opération et signale via une condition associée à l'autre thread qu'il peut travailler via la fonction SDL_CondSignal 

	
Code source tiré de : http://lazyfoo.net/tutorials/SDL/49_mutexes_and_conditions/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_thread.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;
 
//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Création d'une texture vide
		bool createBlank(int width, int height, SDL_TextureAccess = SDL_TEXTUREACCESS_STREAMING);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		//Rendre l'objet self comme cible de rendu
		void setAsRenderTarget();
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
		
		//Pixel manipulators
		bool lockTexture();
		bool unlockTexture();
		void * getPixels();
		void copyPixels(void * pixels);
		int getPitch();
		Uint32 getPixel32(unsigned int x, unsigned int y);
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		void * mPixels;
		int mPitch;
		
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};


// Prototypes fonctions
bool init();
bool loadMedia();
void close();
int producer(void * data);
int consumer(void * data);
void produce();
void consume();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gSplashTexture;

//Protective mutex
SDL_mutex* gBufferLock = NULL;

//Les conditions
SDL_cond* gCanProduce = NULL;
SDL_cond* gCanConsume = NULL;

//The data buffer
int gData = -1;

TTF_Font *gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Conversion de la surface au format d'affichage
	SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
	if(formattedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de convertir la surface en format d'affichage : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une texture vide streamable
	newTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de créer la texture ! SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Permettre le blending
	SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
	
	//Verrouillage de la texture pour la manipulation
	SDL_LockTexture(newTexture, &formattedSurface->clip_rect, &mPixels, &mPitch);
	
	//Copie de la surface de pixels copiée/formattée
	memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	//Dimensions des images
	mWidth = formattedSurface->w;
	mHeight = formattedSurface->h;
	
	//Obtenir les données du pixel dans un format modifiable
	Uint32* pixels = (Uint32*)mPixels;
	int pixelCount = (mPitch/4) * mHeight;
	
	//Map color
	Uint32 colorKey = SDL_MapRGB(formattedSurface->format, 0, 0xFF, 0xFF);
	Uint32 transparent = SDL_MapRGBA(formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00);
	
	//Color key pixel
	for(int i=0; i<pixelCount; i++)
	{
		if(pixels[i] == colorKey)
		{
			pixels[i] = transparent;
		}
	}
	
	//Déverrouillage de la texture pour mise à jour
	SDL_UnlockTexture(newTexture);
	mPixels = NULL;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(formattedSurface); 
	SDL_FreeSurface(loadedSurface); 
	
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

bool LTexture::createBlank(int width, int height, SDL_TextureAccess access)
{
	//Création d'une texture non initialisée
	mTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);
	if(mTexture == NULL)
	{
		fprintf(stderr, "LTexture::createBlank : Impossible de créer la texture vide ! SDL Error : %s\n", SDL_GetError());
	}
	else
	{
		mWidth = width;
		mHeight = height;
	}
	
	return mTexture != NULL;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Set self as render target
void LTexture::setAsRenderTarget()
{
	SDL_SetRenderTarget(gRenderer, mTexture);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Accesseur attribut mPixels
void * LTexture::getPixels()
{
	return mPixels;
}

void LTexture::copyPixels(void * pixels)
{
	//Copie des pixels verrouillés
	memcpy(mPixels, pixels, mPitch * mHeight);
}

//Accesseur mPixels au format Uint32
Uint32 LTexture::getPixel32(unsigned int x, unsigned int y)
{
	//Convert the pixels to 32 bit
	Uint32 * pixels = (Uint32*)mPixels;
	
	//Get the pixel requested
	return pixels[(y * (mPitch/4)) + x];
}

//Accesseur de l'attribut mPitch
int LTexture::getPitch()
{
	return mPitch;
}

bool LTexture::lockTexture()
{
	//La texture est déjà vérrouillée
	if(mPixels != NULL)
	{
		fprintf(stderr, "lockTexture : la texture est déjà vérrouillée\n");
		return false;
	}
	//Vérouillage de la texture
	if(SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
	{
		fprintf(stderr, "lockTexture : Impossible de vérouiller la texture ! %s\n", SDL_GetError());
		return false;
	}
	
	return true;
	
}

bool LTexture::unlockTexture()
{
	if(mPixels == NULL)
	{
		fprintf(stderr, "unlockTexture : La texture n'est pas vérouillée\n");
		return false;
	}
	
	//Déverrouillage de la texture
	SDL_UnlockTexture(mTexture);
	mPixels = NULL;
	mPitch = 0;
	
	return true;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Seed random
	srand(SDL_GetTicks());
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Création du mutex
	gBufferLock = SDL_CreateMutex();
	
	//Création des conditions
	gCanProduce = SDL_CreateCond();
	gCanConsume = SDL_CreateCond();
	
	//Chargement des sprites et textures
	
	//Chargement d'une texture vide
	if(!gSplashTexture.loadFromFile("sprite/splash3.png"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de la texture\n");
		return false;
	}
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{	
	//Libération de la mémoire pour les textures
	gSplashTexture.free();
	
	//Destruction du mutex
	SDL_DestroyMutex(gBufferLock);
	gBufferLock = NULL;
	
	//Desutruction des conditions
	SDL_DestroyCond(gCanProduce);
	SDL_DestroyCond(gCanConsume);
	gCanProduce = NULL;
	gCanConsume = NULL;
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

int producer(void * data)
{
	printf("\nProducer started...\n");
	
	//Seed thread random
	srand(SDL_GetTicks());
	
	//Commencer une production
	for(int i = 0; i<5; i++)
	{
		SDL_Delay(rand()%1000);
		produce();
	}
	
	printf("\nProducer finished\n");
	return 0;
}

int consumer(void * data)
{
	printf("\nConsumer started...\n");
	
	//Seed thread random
	srand(SDL_GetTicks());
	
	//Commencer une production
	for(int i = 0; i<5; i++)
	{
		SDL_Delay(rand()%1000);
		consume();
	}
	
	printf("\nConsumer finished\n");
	return 0;
}

void produce()
{
	//Verrouillage du buffer
	SDL_LockMutex(gBufferLock);
	
	//Si le Buffer est plein
	if(gData != -1)
	{
		//Attendre que le buffer soit vidé
		printf("Producer encounterd full buffer, waiting for consumer to empty buffer\n");
		SDL_CondWait(gCanProduce, gBufferLock);
	}
	
	//Remplir et montrer le buffer
	gData = rand() % 255;
	printf("\nProduced %d\n", gData);
	
	//Déverrouillage
	SDL_UnlockMutex(gBufferLock);
	
	//Signaler la fin de l'opération au consumer
	SDL_CondSignal(gCanConsume);
}

void consume()
{
	//Vérrouillage du buffer
	SDL_LockMutex(gBufferLock);
	
	//Si le buffer est vide
	if(gData == -1)
	{
		//Attend que le buffer soit rempli
		printf("\nConsumer encountered empty buffer, waiting for producer to fill buffer...\n");
		SDL_CondWait(gCanConsume, gBufferLock);
	}
	
	//Montre et vide le buffer
	printf("\nConsumed %d\n", gData);
	gData = -1;
	
	//Déverrouille le buffer
	SDL_UnlockMutex(gBufferLock);
	
	//Signal la fin de l'opération au producer
	SDL_CondSignal(gCanProduce);
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//Création et lancement des threads
	SDL_Thread* producerThread = SDL_CreateThread(producer, "Producer", NULL);
	SDL_Thread* consumerThread = SDL_CreateThread(consumer, "Consumer", NULL);
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de la texture Splash
		gSplashTexture.render(0,0);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	SDL_WaitThread(consumerThread, NULL);
	SDL_WaitThread(producerThread, NULL);
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

